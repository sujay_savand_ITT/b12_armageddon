import numpy as np
from PIL import Image, ImageOps
import tensorflow.keras
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Disable scientific notation for clarity
np.set_printoptions(suppress=True)

model = tensorflow.keras.models.load_model('cnn.h5')

#-------------------------------------------------------------------#
# Create the array of the right shape to feed into the keras model
# The 'length' or number of images you can put into the array is
# determined by the first position in the shape tuple, in this case 1.
#--------------------------------------------------------------------#
data = np.ndarray(shape=(1, 64, 64, 3), dtype=np.float32)

image = Image.open('pneumoniaXray.jpg')

size = (64, 64)
image = ImageOps.fit(image, size, Image.ANTIALIAS)

image_array = np.asarray(image)

image.show()

normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1

# Load the image into the array
data[0] = normalized_image_array

prediction = model.predict(data)
print(prediction)
